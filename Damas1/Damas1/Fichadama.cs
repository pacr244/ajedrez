﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Damas1
{
    public class Ficha
    {
        private byte x;
        private byte y;
        private bool color; 

        public Ficha(byte x, byte y, bool col)
        {
            this.x = x;
            this.y = y;
            this.color = col;
        }
        public byte getX()
        {
            return x;
        }
        public byte getY()
        {
            return y;
        }
        public string getColor()
        {
            string s = "";
            if (color == true)
            {
                s = "rojo";
            }
            if (color == false)
            {
                s = "negro";
            }
            return s;
        }
        public void moverIzq()
        {
            if (color == true)
            {
                x--;
                y++;
            }
            if (color == false)
            {
                x--;
                y--;
            }
        }
        public void moverDer()
        {
            if (color == true)
            {
                x++;
                y++;
            }
            if (color == false)
            {
                x++;
                y--;
            }
        }
    }
}
